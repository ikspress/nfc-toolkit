/*
 * endian.h compatibility shim
 */

#ifndef NFC_TOOLKIT_ENDIAN_H
#define NFC_TOOLKIT_ENDIAN_H

#if __has_include(<sys/param.h>)
#include <sys/param.h>
#else

#define LITTLE_ENDIAN 1234
#define BIG_ENDIAN    4321
#define PDP_ENDIAN    3412

#if defined(__BYTE_ORDER__)
// Use GCC compiler defines to determine endianness
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define BYTE_ORDER LITTLE_ENDIAN
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define BYTE_ORDER BIG_ENDIAN
#elif __BYTE_ORDER__ == __ORDER_PDP_ENDIAN__
#define BYTE_ORDER PDP_ENDIAN
#else
#error Unknown __BYTE_ORDER__
#endif

// Use build system defines to determine endianness
#elif defined(HAVE_LITTLE_ENDIAN)
#define BYTE_ORDER LITTLE_ENDIAN
#elif defined(HAVE_BIG_ENDIAN)
#define BYTE_ORDER BIG_ENDIAN

#else
#error Could not determine endianness
#endif // __BYTE_ORDER__
#endif

#if BYTE_ORDER == LITTLE_ENDIAN

#include <winsock2.h>

#define htobe16(x) htons(x)
#define htole16(x) (x)
#define be16toh(x) ntohs(x)
#define le16toh(x) (x)

#define htobe32(x) ntohl(x)
#define htole32(x) (x)
#define be32toh(x) ntohl(x)
#define le32toh(x) (x)

#define htobe64(x) htonll(x)
#define htole64(x) (x)
#define be64toh(x) ntohll(x)
#define le64toh(x) (x)

#elif BYTE_ORDER == BIG_ENDIAN

#else
#error Unknown BYTE_ORDER
#endif

#endif // NFC_TOOLKIT_ENDIAN_H
