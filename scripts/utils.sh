apply_patch() {
    local _patch
    for _patch in "$@"; do
        patch -Nbp1 -i "${srcdir}/${_patch}"
    done
}

apply_patch_with_fuzz() {
    patch -F${1} -Nbp1 -i "${srcdir}/${2}"
}
