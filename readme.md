# NFC Toolkit

A collection of
[NFC](https://en.wikipedia.org/wiki/Near-field_communication)-related tools for
Windows, compiled with [mingw-w64](https://www.mingw-w64.org).

## Building

If you have a properly set up
[MSYS2 environment](https://www.msys2.org/#installation) and have installed the
necessary
[dependencies](https://www.msys2.org/docs/package-management/#installing-a-package),
you can build the packages located in the [packages](packages) directory by
using the following command:

```bash
makepkg-mingw --skipchecksums
```

## License

nfc-toolkit is licensed under BSD 2-Clause "Simplified" License. For more
details, please refer to [license.md](license.md).
